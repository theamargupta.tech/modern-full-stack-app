import Image from "next/image";
import { Inter } from "next/font/google";
import Paragraph from "@/components/ui/Paragraph";
import LargeHead from "@/components/ui/LargeHead";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  return (
    <main className="bg-red-500">
      <Paragraph size={"sm"}>Some Text</Paragraph>
      <LargeHead size={"sm"}>Some Text</LargeHead>
    </main>
  );
}
