import * as React from 'react'
import { VariantProps, cva } from 'class-variance-authority'
import { cn } from '@/lib/utils'

export const largeHeadVariants = cva(
  'text-black dark:text-white text-center lg:text-left font-extrabold leading-tight tracking-tighter',
  {
    variants: {
      size: {
        default: 'text-4xl md:text-5xl lg:text-6xl',
        lg: 'text-3xl md:text-4xl lg:text-5xl',
        sm: 'text-2xl md:text-3xl lg:text-4xl',
      },
    },
    defaultVariants: {
      size: 'default',
    },
  }
)

interface LargeHeadProps
  extends React.HTMLAttributes<HTMLHeadingElement>,
    VariantProps<typeof largeHeadVariants> {}

const LargeHead = React.forwardRef<HTMLHeadingElement, LargeHeadProps>(
  ({ className, size, children, ...props }, ref) => {
    return (
      <h1
        ref={ref}
        {...props}
        className={cn(largeHeadVariants({ size, className }))}>
        {children}
      </h1>
    )
  }
)

LargeHead.displayName = 'LargeHead'

export default LargeHead